from typing_extensions import Self
from django.db import models
from common import models
from user.models import UserModel
# Create your models here.

def product(BaseModel):
    user=models.ForeignKey(BaseModel)
    Category=models.CharField(max_length=50,null=False)
    Name=models.CharField(max_length=20,null=False)
    price=models.FloatField(null=False)
    description=models.TextField()
    is_feature=models.BooleanField(default=True)
    requested_credit=models.IntegerField(max_length=20)

def category(BaseModel):
    title=models.CharField(max_length=20,null=False)
    is_active=models.ForeignKey(UserModel)
    is_parent=models.BooleanField(default=True)
    parent_category=models.ForeignKey(Self)