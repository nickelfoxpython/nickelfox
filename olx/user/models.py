from django.db import models
from common import models
# Create your models here.
#phone Charfield,phone regex validation
class UserModel(common.BaseModel):
    buyer='buyer'
    seller='seller'
    admin='admin'
    name=models.CharField(max_length=50)
    phone=models.IntegerField(max_length=10,null=False,default=0)
    user_role=models.CharField(choices=UserModel.choices)
    password=models.SlugField(max_length=20,allow_unicode=True)
    is_active=models.BooleanField(default=True)
    is_admin=models.BooleanField(default=False)


