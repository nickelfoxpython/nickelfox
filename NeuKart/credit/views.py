from email.mime import image
import os
from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext
from credit import models
from django.core.mail import BadHeaderError, send_mail
from django.http import HttpResponse, HttpResponseRedirect# Create your views here.
from NeuKart import views
from django.views.generic import (TemplateView, ListView, CreateView, DetailView, FormView)
from django.contrib import messages
from django.urls import reverse
from django.views import View
from .forms import CreditForm
from credit.models import Credits
from django.forms import modelformset_factory
from django.apps import AppConfig
import pickle
import collections

class CreditRequest(View):
    form_class=CreditForm
    initial={'key':'Value'}
    template_name='credit.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form = CreditForm(request.POST)
            model = form.save(commit=False)
            model.user = request.user
            model.point=request.POST["point"]
            model.is_approved=False


            model.save()
            return HttpResponseRedirect('/',RequestContext(request))
        else:
            print(0)
            form=CreditForm()    
        return render(request, self.template_name,{'form':form})


from django.db.models import Q,Case,When,Sum

class Creditprofile(View):
    sum=0
    model=Credits
    template_name='creditProfile.html'
    def get(self, request, *args, **kwargs):
        user=Credits.objects.filter(user=request.user)
        point=user.aggregate(credit=Sum('point'))

        return render(request,self.template_name,{'credit':str(point).replace("{","").replace("}", "")})




# class Creditprofile(View):
#     sum=0
#     model=Credits
#     template_name='creditProfile.html'
#     def get(self, request, *args, **kwargs):
#         user=Credits.objects.all().get(user=request.user)
#         if user.is_approved=="No":        
#             return render(request,self.template_name)
#         else:
#             return render(request,self.template_name,{'credit':user.point}) 