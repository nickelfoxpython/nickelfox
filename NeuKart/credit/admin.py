from django.contrib import admin
from credit.models import Credits

# Register your models here.
class creditAdmin(admin.ModelAdmin):
    list_display=('user','point','is_approved')

admin.site.register(Credits,creditAdmin)
