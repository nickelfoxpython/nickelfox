from django.db import models
from django.shortcuts import render
from common.models import BaseModel
from django.contrib.auth.models import User
# from NeuKart.views import user
# Create your models here.

class Credits(BaseModel):
    user=models.ForeignKey(User,on_delete=models.CASCADE,default=None)
    point=models.IntegerField(default=0) 
    availability = (("Yes", "Yes"), ("No", "No"))
    is_approved = models.CharField(max_length=100, choices=availability)

# #classfied
