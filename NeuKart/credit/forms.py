
from credit.models import Credits
from django.forms import ModelForm

class CreditForm(ModelForm):
    class Meta:
        model=Credits
        fields=('point',)

class UpdateCreditForm(ModelForm):
    class Meta:
        model=Credits
        fields='__all__'
# form=CreditForm()        
# credit=CreditRequest.objects.get()
# form=CreditForm(instance=credit)        
