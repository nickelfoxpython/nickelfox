from tkinter.tix import Tree
from django.db import models
import uuid
# Create your models here.
class BaseModel(models.Model):
    class Meta:
        abstract=True
    objects = models.Manager()
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
