from django.urls import path
from user import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    # fotoblog/urls.py

    path('', views.LoginPageView.as_view(), name='login'),
    # path('register/', views.register_request, name="register"),
    # path('login/', views.login_request, name="login"),
    # path("logout", views.logout_request, name= "logout"),
    path('SignUpView', views.SignUpView.as_view(), name="SignUpView"),
    # path('login', views.handeLogin.as_view(), name="handleLogin"),
    # path('logout', views.handelLogout.as_view(), name="handleLogout"),
    # authentication/urls.py
   path('', LoginView.as_view(
           template_name='login.html',
           redirect_authenticated_user=True),
        name='login'),

]