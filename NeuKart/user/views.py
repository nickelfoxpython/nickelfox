from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib import messages
from django.views import View
from django.contrib.auth  import authenticate,  login, logout
from django.views.generic import View
from django.conf import settings
from django.contrib.auth import login
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from . import forms


from .forms import UserRegisterForm
from django.views.generic.edit import CreateView

class SignUpView(SuccessMessageMixin, CreateView):
  template_name = 'register.html'
  success_url = reverse_lazy('login')
  form_class = UserRegisterForm
  success_message = "Your profile was created successfully"

class LoginPageView(View):
    template_name = 'login.html'
    form_class = forms.UserRegisterForm
    
    def get(self, request):
        form = self.form_class()
        message = ''
        return render(request, self.template_name, context={'form': form, 'message': message})
        
    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
            )
            if user is not None:
                login(request, user)
                return redirect('home.html')
        message = 'Login failed!'
        return render(request, self.template_name, context={'form': form, 'message': message})
    def logout_user(request):
        logout(request)
        return redirect('login.html')    



 # from django.contrib.auth import login, logout, authenticate
# from django.shortcuts import redirect, render
# from django.contrib import messages
# from django.views.generic import CreateView
# from .forms import CustomerSignUpForm, EmployeeSignUpForm
# from django.contrib.auth.forms import AuthenticationForm
# from .models import User


# def register(request):
#     return render(request, 'register.html')


# class customer_register(CreateView):
#     model = User
#     form_class = CustomerSignUpForm
#     template_name = 'customer_register.html'

#     def form_valid(self, form):
#         user = form.save()
#         login(self.request, user)
#         return redirect('/')


# class employee_register(CreateView):
#     model = User
#     form_class = EmployeeSignUpForm
#     template_name = 'employee_register.html'

#     def form_valid(self, form):
#         user = form.save()
#         login(self.request, user)
#         return redirect('/')


# def login_request(request):
#     if request.method == 'POST':
#         form = AuthenticationForm(data=request.POST)
#         if form.is_valid():
#             username = form.cleaned_data.get('username')
#             password = form.cleaned_data.get('password')
#             user = authenticate(username=username, password=password)
#             if user is not None:
#                 login(request, user)
#                 return redirect('/')
#             else:
#                 messages.error(request, "Invalid username or password")
#         else:
#             messages.error(request, "Invalid username or password")
#     return render(request, 'login.html',
#                   context={'form': AuthenticationForm()})


# def logout_view(request):
#     logout(request)
#     return redirect('/')
       