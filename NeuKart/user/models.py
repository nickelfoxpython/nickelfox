from django.db import models
from django.contrib.auth.models import (AbstractBaseUser,BaseUserManager)
from common.models import BaseModel
from django.utils.translation import ugettext_lazy as _

class UserManager(BaseUserManager):
    def create_user(self,email,password=None,is_admin=False):
        if not email:
            raise ValueError('email is required')
        user_obj=self.model(
            email=self.normalize_email(email)
        )    
        user_obj.set_password(password)
        user_obj.email=email
        user_obj.admin=is_admin
        user_obj.save(using=self._db)
        return user_obj

    def create_superuser(self,email,password=None):
        user=self.create_user(
            email,
            password=password,
            is_admin=True
        )    
        return user
class User(AbstractBaseUser,BaseModel):
    email=models.EmailField(max_length=255,unique=True)
    full_name=models.CharField(max_length=255,blank=False,null=True)
    admin=models.BooleanField(default=False)
    active=models.BooleanField(default=True)
    
    
    objects=UserManager()

    USERNAME_FIELD='email'

    def __str__(self):
        return self.email
    def get_full_name(self):
        return self.full_name
    def is_active(self):
        return self.active 
    def is_admin(self):
        return self.admin         
