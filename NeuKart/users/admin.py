from django.contrib import admin
from users.models import user
# Register your models here.
class userAdmin(admin.ModelAdmin):
    list_display=('name','phone','password','is_active','is_admin')
admin.site.register(user,userAdmin)    