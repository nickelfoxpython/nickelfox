from tkinter import CASCADE
from django.db import models
from common.models import BaseModel
from NeuKart import views
# Create your models here.
class user(BaseModel):
    username=models.CharField(max_length=50)
    name=models.CharField(max_length=50)
    phone=models.CharField(max_length=20)
    password=models.CharField(max_length=20)
    is_active=models.BooleanField(default=True)
    is_admin=models.BooleanField(default=False)
