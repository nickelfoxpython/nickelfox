from django.contrib import admin

from products.models import Product
from products.models import Offer
# from products.models import sells
from products.models import Category
# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display=('product_name','category','subcategory','description','price','image')
class offerAdmin(admin.ModelAdmin):
    list_display=('product','price','is_approved')
# class sellsAdmin(admin.ModelAdmin):
#     list_display=('image','Product','category','subcategory','price','description')
class CategoryAdmin(admin.ModelAdmin):
    list_display=('title','is_active',)    


admin.site.register(Offer,offerAdmin)        
admin.site.register(Product,ProductAdmin) 
# admin.site.register(sells,sellsAdmin)  
admin.site.register(Category,CategoryAdmin) 
