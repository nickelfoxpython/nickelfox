from django.forms import ModelForm
from products.admin import offerAdmin
from products.models import Product,Offer

class ProductForm(ModelForm):
    class Meta:
        model=Product
        fields=('product_name','category','subcategory','description','price',)
class OfferForm(ModelForm):
    class Meta:
        model=Offer
        fields=('product','price',)
# form=ProductForm()
# offerform=OfferForm()
# offer=Offer.objects.all(pk=1)
# offerform=OfferForm(instance=offer)
# product=Product.objects.get(pk=1)
# form=ProductForm(instance=product)        

