# from unicodedata import Category
from django.db import models
from common.models import BaseModel
from django.contrib.auth.models import User
# from user.models import User
from django.contrib.auth.models import User
#user foreignkey
# Create your models here.
#category should be predefined
#offer model
class Category(BaseModel):    
    title=models.CharField(max_length=50,default='electronics')
    is_active=models.BooleanField(default=True)

# class Subcategory(BaseModel):
#     category=models.ForeignKey(Category,on_delete=models.CASCADE)
#     title=models.CharField(max_length=40)


class Product(BaseModel):
    product_name=models.CharField(max_length=50,default=None)
    category=models.ForeignKey(Category,null=True,blank=True,on_delete=models.CASCADE)
    subcategory=models.CharField(max_length=100,blank=True)
    # subcategory=models.ForeignKey(Subcategory,null=True,blank=True,on_delete=models.CASCADE)
    seller=models.ForeignKey(User,on_delete=models.CASCADE,default=None)
    description=models.TextField(max_length=100)
    price=models.FloatField(default=None)
    image=models.ImageField(upload_to='img')
    

class Offer(BaseModel):
    product=models.ForeignKey(Product,on_delete=models.CASCADE,default=None) 
    # user=models.ForeignKey(User,on_delete=models.CASCADE)
    price=models.FloatField()
    is_approved=models.BooleanField(default=False)



