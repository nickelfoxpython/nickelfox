# from django.contrib import admin
from django.urls import path,include
from NeuKart import urls
from products import views
from products.views import *

urlpatterns=[
    # path('',views.FeaturedProduct,name='FeaturedProduct'),
    path('ViewProduct/',views.ViewProduct,name='ViewProduct'),
    path('offer/',views.offer.as_view(),name='offer'),
    path('ViewProductApi',views.ViewProductApi.as_view(),name='ViewProductApi'),
    path('ProductApi',views.ProductApi.as_view(),name='ProductApi')
]