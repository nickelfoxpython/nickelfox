from django.db import models
from common.models import BaseModel
from django.contrib.auth.models import User
# Create your models here.

class CreditRequest(BaseModel):
    user=models.CharField(max_length=50,default=None)
    # user=models.ForeignKey(User,on_delete=models.CASCADE)
    point=models.IntegerField() 
    is_approved=models.BooleanField(default=False)

# #classfied
