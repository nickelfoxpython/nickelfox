from unicodedata import name
from django.urls import path,include
from NeuKart import urls
from credit import views

urlpatterns=[
path('',views.credit,name='credit'),
path('email',views.email,name='email'),
# path('sell',views.sell,name='sell'),
# path('sellsave',views.sellsave,name='sellsave'),
path('editcredit',views.editcredit,name='editcredit')
]