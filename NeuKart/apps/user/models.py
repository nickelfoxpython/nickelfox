from django.db import models
from django.contrib.auth.models import (AbstractBaseUser,BaseUserManager)
from common.models import BaseModel
from credit.models import CreditRequest
from django.utils.translation import ugettext_lazy as _

class UserManager(BaseUserManager):
    def create_user(self,email,password=None,is_admin=False):
        if not email:
            raise ValueError('email is required')
        user_obj=self.model(
            email=self.normalize_email(email)
        )    
        user_obj.set_password(password)
        user_obj.email=email
        user_obj.admin=is_admin
        user_obj.save(using=self._db)
        return user_obj

    def create_superuser(self,email,password=None):
        user=self.create_user(
            email,
            password=password,
            is_admin=True
        )    
        return user
class User(AbstractBaseUser,BaseModel):
    email=models.EmailField(max_length=255,unique=True)
    full_name=models.CharField(max_length=255,blank=False,null=True)
    admin=models.BooleanField(default=False)
    active=models.BooleanField(default=True)
    
    
    objects=UserManager()

    USERNAME_FIELD='email'

    def __str__(self):
        return self.email
    def get_full_name(self):
        return self.full_name
    def is_active(self):
        return self.active 
    def is_admin(self):
        return self.admin         
# class Task(models.Model):
#     class Meta:
#         permissions = [
#             ("change_task_status", "Can change the status of tasks"),
#             ("close_task", "Can remove a task by setting its status as closed"),
#         ]          
#     UserManager.create_user('app.close_task')    
# from email.policy import default
# from django.db import models
# from django import forms
# # from common.models import BaseModel
# # #abstract base user
# # #camel case and no plur
# # #no default
# # #no role,admin and consumer
# # #credit balance export from credit

# # # Create your models here.
# # class users(BaseModel):
# #     role = (
# #         ('buyer', 'buyer'),
# #         ('seller', 'seller'),
# #     )
    
# #     role = models.CharField(choices=role, max_length=8, default='buyer')
# #     def __str__(self):
# #         return self.user.username

# from django.contrib.auth.models import AbstractBaseUser,BaseUserManager,User,PermissionsMixin
# from django.forms import BooleanField, CharField, EmailField
# from credit import models
# from user import forms
# class CustomUserManager(BaseUserManager):
#     def create_user(self,email,user_name,first_name,last_name,password):
#         if not email:
#             raise ValueError('user must have email')
#         if not user_name:
#             raise ValueError('please enter usernmae')    
#         if not first_name:
#             raise ValueError('user must have first name')
#         if not last_name:
#             raise ValueError('user must have last name')
#         email=self.normalize_email(email)
#         user=self.model(email=email,user_name=user_name,first_name=first_name,last_name=last_name)
#         user.set_password(password)
#         user.save()
#         return user    
#         # user=self.model(
#         #     email=self.normalize_email(email),
#         #     first_name=first_name,
#         #     last_name=last_name,
#         # )                
#         # user.set_password(raw_password=password)
#         # user.save(using=self._db)

# class CustomUser(AbstractBaseUser,PermissionsMixin):
#     '''Model representation for user'''
#     first_name=models.CharField(max_length=50,null=False)
#     last_name=models.CharField(max_length=50,unique=True)
#     user_name=models.CharField(max_length=150,unique=True)
#     email=models.EmailField(max_length=250)
#     is_active=models.BooleanField(default=True)
#     is_admin=models.BooleanField(default=False)
#     objects=CustomUserManager()

#     REQUIRED_FIELD=['user_name','first_name','last_name']

#     USERNAME_FIELD='email'

#     EMAIL_FIELD='email'

#     def __str__(self):
#         return self.email

#     def get_short_name(self):
#         return self.first_name

#     def get_full_name(self):
#         first=self.first_name
#         last=self.last_name
#         fullname=str(first) +' '+ str(last)       
#         return fullname