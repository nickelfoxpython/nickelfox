from django.urls import path
from user import views

urlpatterns = [
    # path('register/', views.register_request, name="register"),
    # path('login/', views.login_request, name="login"),
    # path("logout", views.logout_request, name= "logout"),
    path('signup', views.handleSignUp.as_view(), name="register_request"),
    path('login', views.handeLogin.as_view(), name="handleLogin"),
    path('logout', views.handelLogout.as_view(), name="handleLogout"),
]