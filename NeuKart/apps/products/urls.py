# from django.contrib import admin
from django.urls import path,include
from NeuKart import urls
from products import views

urlpatterns=[
    path('',views.feature,name='feature'),
    path('prod/',views.prod,name='prod'),
    path('offer/',views.offer,name='offer'),
    path('sell',views.sell,name='sell'),
    path('sellsave',views.sellsave,name='sellsave')
]