# from unicodedata import Category
from django.db import models
from common.models import BaseModel
# from django.contrib.auth.models import User
from user.models import User
from django.contrib.auth.models import User
#user foreignkey
# Create your models here.
#category should be predefined
#offer model

class Product(BaseModel):
    product=models.CharField(max_length=50,null=False)
    category=models.CharField(max_length=50,null=True)
    subcategory=models.CharField(max_length=50)
    # seller=models.CharField(max_length=50)
    user=models.CharField(max_length=50,default=None)
    # seller=models.ForeignKey(User,default=None,on_delete=models.CASCADE)
    description=models.TextField(max_length=100)
    price=models.FloatField(default=None)
    image=models.ImageField(upload_to='img')
    is_featured=models.BooleanField(default=False)

class Offer(BaseModel):
    product=models.ForeignKey(Product,on_delete=models.CASCADE, blank=True, null=True) 
    # product=models.ForeignKey(Product, on_delete=models.CASCADE,default=None)    
    # user=models.ForeignKey(User,on_delete=models.CASCADE,default=None)
    user=models.CharField(max_length=50,default=None)
    price=models.FloatField(default=None)
    is_approved=models.BooleanField(default=False)

class City(BaseModel):
        name = models.CharField(max_length=50)
        country = models.ForeignKey("Country",on_delete=models.CASCADE,default=None)
        def __unicode__(self):
            return u'%s' % (self.name)

class Country(models.Model):
    name = models.CharField(max_length=50)
    def __unicode__(self):
        return u'%s' % (self.name)

class Category(BaseModel):
    category_choices=(
        ('electronics','electronics'),
        ('Kitchen appliances','Kitchen appliances'),
        ('clothes','clothes'),
        ('real estate','real estate'),
        ('automobiles','automobiles'),
        ('stationary','stationary')
    )
        
    title=models.CharField(max_length=50,choices=category_choices)
    is_active=models.BooleanField(default=True)
    is_parent=models.BooleanField(default=True)

    def __str__(self):
        return self.title

class Subcategory(BaseModel):
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    title=models.CharField(max_length=40)

    def __str__(self):
        return self.title

# class sells(BaseModel):
#     image=models.ImageField(null=True,default=None)
#     Product=models.CharField(max_length=50,default=None)
#     seller=models.ForeignKey(User,on_delete=models.CASCADE)
#     category=models.ForeignKey(Category,on_delete=models.SET_NULL, blank=True, null=True)
#     subcategory=models.ForeignKey(Subcategory,on_delete=models.SET_NULL, blank=True, null=True)
#     price=models.FloatField(default=None)
#     description=models.TextField(max_length=50,default=None)

#     def __str__(self):
#         return self.Product
