from django.contrib import admin

from products.models import Product
from products.models import Offer
# from products.models import sells
from products.models import Category,City,Country
# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    list_display=('product','category','subcategory','description','price','image','is_featured')
class offerAdmin(admin.ModelAdmin):
    list_display=('product','price','is_approved')
# class sellsAdmin(admin.ModelAdmin):
#     list_display=('image','Product','category','subcategory','price','description')
class CategoryAdmin(admin.ModelAdmin):
    list_display=('title','is_active','is_parent',)    
class CityAdmin(admin.ModelAdmin):
    list_display=('name','country',)
class CountryAdmin(admin.ModelAdmin):
    list_display=('name',)        
admin.site.register(Offer,offerAdmin)        
admin.site.register(Product,ProductAdmin) 
# admin.site.register(sells,sellsAdmin)  
admin.site.register(Category,CategoryAdmin) 
admin.site.register(City,CityAdmin)
admin.site.register(Country,CountryAdmin)