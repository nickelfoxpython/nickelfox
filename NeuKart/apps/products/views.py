import os
import imp
from unicodedata import name
from django.shortcuts import render
from django.http import HttpResponse
from django.core.paginator import Paginator
from products.models import Product
from credit.models import CreditRequest
from django.contrib.auth.models import User
# from products.forms import CategoryForm
from django.db import models
from products.models import *
import json as simplejson

def sellproduct(request):
    productFeatured=Product.objects.filter(is_featured=True)
    data={
        'productFeatured':productFeatured
    }
    if CreditRequest.is_approved:
        return render(request,'featured.html',data)    
def feature(request):
    productFeatured=Product.objects.filter(is_featured=True)
    data={
            'productFeatured':productFeatured
        }
    return render(request,'featured.html',data)
    # if sells.point >= sells.price/10:
    #     return render(request,'featured.html',data)
    # else :
    #     return render(request,'product.html',data)    
def prod(request):
    # productFeatured=Product.objects.filter(is_featured=True).order_by('name')
    ServiceData=Product.objects.all()
    if request.method=='GET':
        st=request.GET.get('servicename')
        if st!=None:
            ServiceData=Product.objects.filter(name__icontains=st)
            ServiceData=Product.objects.filter(seller__icontains=st)
            ServiceData=Product.objects.filter(category__icontains=st)
            ServiceData=Product.objects.filter(subcategory__icontains=st)
    return render(request,'prod.html')    
def sell(request):
    return render(request,'sell.html') 
def sellsave(request):
    if request.method=="POST":
        Name=request.POST.get('Name')
        point=request.POST.get('point')
        sellername=request.User
        category=request.POST.get('category')
        subcategory=request.POST.get('subcategory')
        price=request.POST.get('price')
        Description=request.POST.get('Description')
        ss=Product(Name=Name,point=point,seller=sellername,category=category,subcategory=subcategory,price=price,description=Description)
        ss.save()
    return render(request,'sell.html')
    # for a in ServiceData:
    #     print(a.name)
    # return HttpResponse("Hello this is a product page")
    # dataf={
    #     'productFeatured':productFeatured
    # }
    data={
        'ServiceData':ServiceData
    }
    return render(request,'prod.html',data)
def offer(request):
    if request.method=="POST":
        price=request.POST.get('price')
        offersave=Offer(price=price)
        offersave.save()
    return render(request,'offer.html')



def index(request):
    countries = Country.objects.all()
    print (countries)
    return render(request, 'prod.html', {'countries': countries})

def getdetails(request):
    #country_name = request.POST['country_name']
    country_name = request.GET['cnt']
    print ("ajax country_name "), country_name

    result_set = []
    all_cities = []
    answer = str(country_name[1:-1])
    selected_country = Country.objects.get(name=answer)
    print ("selected country name "), selected_country
    all_cities = selected_country.city_set.all()
    for city in all_cities:
        print ("city name"), city.name
        result_set.append({'name': city.name})
    return HttpResponse(simplejson.dumps(result_set), mimetype='application/json',     content_type='application/json')    