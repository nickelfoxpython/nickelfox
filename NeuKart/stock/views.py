from dataclasses import dataclass
from msilib.schema import Class, ListView
from pyexpat import model
from re import template
from django.views.generic import (TemplateView, ListView, CreateView, DetailView, FormView,RedirectView)
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.views import View
from .models import *
from django.template import RequestContext
from .forms import *
from credit.models import *
from .task import *
from django.db.models import Q
from django.core.paginator import Paginator
from django.template import RequestContext
from credit.forms import *
from .models import offer
# Create your views here.

# def changecredit(request):
      


class gallery(View):
    model=Products
    template_name='gallery.html'
    def get(self, request, *args, **kwargs):
        ServiceData=Products.objects.all()
        return render(request,self.template_name,{'ServiceData':ServiceData})


class Featuredgallery(View):
    model=featured
    template_name='featuredGallery.html'
    def get(self, request, *args, **kwargs):
        #change the logic
        FeatureData=featured.objects.all().filter(is_approved='Yes')
        paginator=Paginator(FeatureData,20)
        page_number=request.GET.get('page')
        FeatureData=paginator.get_page(page_number)
        if request.method=="GET":
            st=request.GET.get('servicename')
            if st!=None:
                FeatureData=featured.objects.filter(Q(Product_name__icontains=st))
            data={'FeatureData':FeatureData}   
        return render(request,self.template_name,data)

# class offerupdate(View):
#     template_name='offerupdate.html'
#     def get(self, request, *args, **kwargs):
#         ServiceData=offer.objects.all()
#         return render(request,self.template_name,{'ServiceData':ServiceData})



class featuredProduct(View):
    form_class=featuredProductForm
    initial={'key':'value'}
    template_name='featuredProduct.html'
    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            print(1)
            form = featuredProductForm(request.POST)
            model = form.save(commit=False)
            model.Product_name=request.POST["Product_name"]
            model.user = request.user
            model.category=form.cleaned_data["category"]
            model.Subcategory=form.cleaned_data["Subcategory"]
            model.price=request.POST["price"]
            model.description=request.POST["description"]
            model.is_approved=False
            # model.image=request.POST["image"]
            model.save()
            return HttpResponseRedirect('/',RequestContext(request))
        else:
            print(0)
            form=featuredProductForm()    
        return render(request, self.template_name,{'form':form})


class Offers(View):
    form_class=offerForm
    initial={'key':'value'}
    template_name='offer.html'
    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form = offerForm(request.POST)
            model = form.save(commit=False)
            model.user = request.user
            model.product_name=form.cleaned_data['product_name']
            model.price=request.POST["price"]
            model.is_approved=False
            model.save()
            return HttpResponseRedirect('/',RequestContext(request))
        else:
            print(0)
            form=featuredProductForm()    
        return render(request, self.template_name,{'form':form})

        
    
class Approval(View):
    # model=AdminApproval
    template_name='Approval.html'
    def get(self, request, *args, **kwargs):
        return render(request,self.template_name)


class AddCategory(View):
    form_class=AddCategoryForm
    initial={'key':'value'}
    template_name='Approval.html'
    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form = AddCategoryForm(request.POST)
            model = form.save(commit=False)
            model.title=request.POST["title"]
            model.is_active=form.cleaned_data["is_active"]
            # model.image=request.POST["image"]
            model.save()
            return HttpResponseRedirect('/',RequestContext(request))
        else:
            form=AddCategoryForm()    
        return render(request, self.template_name,{'form':form})


class AddSubcategory(View):
    form_class=AddSubcategoryForm
    initial={'key':'value'}
    template_name='Approval.html'
    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form = AddSubcategoryForm(request.POST)
            model = form.save(commit=False)
            model.category=form.cleaned_data['category']
            model.title=request.POST["title"]
            # model.image=request.POST["image"]
            model.save()
            return HttpResponseRedirect('/',RequestContext(request))
        else:
            form=AddSubcategoryForm()    
        return render(request, self.template_name,{'form':form})
#class based
class viewCategory(View):
    # model=Credits,Category,Subcategory,offer
    template_name='status.html'
    def get(self, request, *args, **kwargs):
        credit=Credits.objects.all()
        viewcategory=Category.objects.all()
        viewsubcategory=Subcategory.objects.all()
        viewoffer=offer.objects.all()
        user=User.objects.all()
        return render(request,self.template_name,{'credit':credit,'viewcategory':viewcategory,'viewsubcategory':viewsubcategory,'viewoffer':viewoffer,'user':user})

class delete_user(RedirectView):
    url='/'
    def get_redirect_url(self, *args, **kwargs):
        del_id=kwargs['id']
        User.objects.get(pk=del_id).delete()
        return super().get_redirect_url(*args,**kwargs)


class delete_data(RedirectView):
    url='/'
    def get_redirect_url(self, *args, **kwargs):
        del_id=kwargs['id']
        Credits.objects.get(pk=del_id).delete()
        return super().get_redirect_url(*args,**kwargs)

class deleteoffer(RedirectView):
    url='/'
    def get_redirect_url(self, *args, **kwargs):
        del_id=kwargs['id']
        offer.objects.get(pk=del_id).delete()
        return super().get_redirect_url(*args,**kwargs)


class delete_category(RedirectView):
    url='/'
    def get_redirect_url(self, *args, **kwargs):
        del_id=kwargs['id']
        Category.objects.get(pk=del_id).delete()
        return super().get_redirect_url(*args,**kwargs)


class delete_subcategory(RedirectView):
    url='/'
    def get_redirect_url(self, *args, **kwargs):
        del_id=kwargs['id']
        Subcategory.objects.get(pk=del_id).delete()
        return super().get_redirect_url(*args,**kwargs)


class offerUpdate(View):
    def get(self,request,id):
        pi=offer.objects.get(pk=id)
        fm=offerUpdateForm(instance=pi)
        return render(request,'offerupdate.html',{'form':fm})
    def post(self,request,id):
        pi=offer.objects.get(pk=id)
        fm=offerUpdateForm(request.POST,instance=pi)
        if fm.is_valid():
            fm.save()
        return render(request,'offerupdate.html',{'form':fm})        


class UpdateCredit(View):
    def get(self,request,id):
        pi=Credits.objects.get(pk=id)
        fm=UpdateCreditForm(instance=pi)
        return render(request,'updatecredit.html',{'form':fm})
    def post(self,request,id):
        pi=Credits.objects.get(pk=id)
        fm=UpdateCreditForm(request.POST,instance=pi)
        if fm.is_valid():
            fm.save()
        return render(request,'updatecredit.html',{'form':fm})        

class Updatesubcategory(View):
    def get(self,request,id):
        pi=Subcategory.objects.get(pk=id)
        fm=AddSubcategoryForm(instance=pi)
        return render(request,'updatesubcategory.html',{'form':fm})
    def post(self,request,id):
        pi=Subcategory.objects.get(pk=id)
        fm=AddSubcategoryForm(request.POST,instance=pi)
        if fm.is_valid():
            fm.save()
        return render(request,'updatesubcategory.html',{'form':fm})                

class viewoffer(View):
    def get(self,request):
        pi=offer.objects.all().filter(user=request.user)
        template_name='viewoffer.html'
        # fm=offerUpdateForm(instance=pi)
        return render(request,template_name,{'form':pi})