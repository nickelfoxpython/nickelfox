# Generated by Django 3.2.12 on 2022-03-14 05:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0002_auto_20220314_1031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='Seller',
            field=models.CharField(max_length=100),
        ),
    ]
