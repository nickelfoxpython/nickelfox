# Generated by Django 3.2.12 on 2022-03-14 07:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0004_auto_20220314_1201'),
    ]

    operations = [
        migrations.RenameField(
            model_name='featuredproduct',
            old_name='Seller',
            new_name='user',
        ),
        migrations.RemoveField(
            model_name='featuredproduct',
            name='image',
        ),
    ]
