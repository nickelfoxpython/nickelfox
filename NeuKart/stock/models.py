from email.mime import image
from email.policy import default
from tkinter import CASCADE
from turtle import title
from unicodedata import category
from django.db import models
from common.models import BaseModel
from django.contrib.auth.models import User

# Create your models here.
class Category(BaseModel):
    title=models.CharField(max_length=50)
    is_active=models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.title)

class Subcategory(BaseModel):
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    title=models.CharField(max_length=100)

    def __str__(self):
        return "{}".format(self.title)

#category and subcategory foriegnkey
class Products(BaseModel):
    product_name=models.CharField(max_length=100,default=None)
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    subcategory=models.ForeignKey(Subcategory,on_delete=models.CASCADE)
    Seller=models.CharField(max_length=100)
    description=models.TextField(max_length=100)
    price=models.FloatField(default=None)


    def __str__(self):
        return self.product_name            

class featured(BaseModel):
    Product_name=models.CharField(max_length=100)
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    Subcategory=models.ForeignKey(Subcategory,on_delete=models.CASCADE)
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    price=models.FloatField()
    description=models.TextField(max_length=200)
    availability = (("Yes", "Yes"), ("No", "No"))
    is_approved = models.CharField(max_length=100, choices=availability,default="Yes")
    
class offer(BaseModel):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    product_name=models.ForeignKey(Products,on_delete=models.CASCADE,default=None)
    price=models.IntegerField()
    availability = (("Yes", "Yes"), ("No", "No"))
    is_approved = models.CharField(max_length=100, choices=availability)

    def __str__(self):
        return "{}".format(self.product_name)


