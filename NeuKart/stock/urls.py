# from django.contrib import admin
from unicodedata import name
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from stock import views


urlpatterns=[
    path('',views.gallery.as_view(),name='gallery'),
    path('featuredProduct',views.featuredProduct.as_view(),name='featuredProduct'),
    path('Offer',views.Offers.as_view(),name='Offer'),
    path('Featuredgallery',views.Featuredgallery.as_view(),name='Featuredgallery'),
    path('Approval',views.Approval.as_view(),name='Approval'),
    path('AddCategory',views.AddCategory.as_view(),name="AddCategory"),
    path('AddSubcategory',views.AddSubcategory.as_view(),name="AddSubcategory"),
    path('viewCategory',views.viewCategory.as_view(),name="viewCategory"),
    path(r'^deleteu/(?P<id>[\w-]+)/$',views.delete_user.as_view(),name="delete_user"),
    path(r'^delete/(?P<id>[\w-]+)/$',views.delete_data.as_view(),name="delete_data"),
    path(r'^deleteo/(?P<id>[\w-]+)/$',views.deleteoffer.as_view(),name='deleteoffer'),
    path(r'^deletec/(?P<id>[\w-]+)/$',views.delete_category.as_view(),name="delete_category"),
    path(r'^deletes/(?P<id>[\w-]+)/$',views.delete_subcategory.as_view(),name="delete_subcategory"),
    path(r'^(/?P<id>[\w-]+)/$',views.UpdateCredit.as_view(),name="UpdateCredit"),
    path(r'^updateo/(?P<id>[\w-]+)/$',views.offerUpdate.as_view(),name="offerUpdate"),
    path(r'^Updatesubcategory/(?P<id>[\w-]+)/$',views.Updatesubcategory.as_view(),name="Updatesubcategory"),
    path('viewoffer',views.viewoffer.as_view(),name='viewoffer'),

] +  static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)