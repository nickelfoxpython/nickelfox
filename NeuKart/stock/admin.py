from django.contrib import admin
from .models import *
# Register your models here.
class CategoryAdmin(admin.ModelAdmin):
    list_display=('title','is_active')

class SubcategoryAdmin(admin.ModelAdmin):
    list_dsiplay=('category','title')

class ProductsAdmin(admin.ModelAdmin):
    list_display=('product_name','category','subcategory','Seller','description','price',)    

class featuredProductAdmin(admin.ModelAdmin):
    list_display=('Product_name','category','Subcategory','user','description','price','is_approved',)

class offerAdmin(admin.ModelAdmin):
    list_display=('user','product_name','price','is_approved',)

admin.site.register(Category,CategoryAdmin)
admin.site.register(Subcategory,SubcategoryAdmin)
admin.site.register(Products,ProductsAdmin)            
admin.site.register(featured,featuredProductAdmin)
admin.site.register(offer,offerAdmin)
