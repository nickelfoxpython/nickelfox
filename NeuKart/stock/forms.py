from dataclasses import fields
from django.forms import ModelForm
from .models import *
from .admin import *

class featuredProductForm(ModelForm):
    class Meta:
        model=featured
        exclude=('deleted_at','user','is_approved',)
        # fields='__all__'
        # fields=('Product_name','category','Subcategory','Seller','price','description','image',)

class offerForm(ModelForm):
    class Meta:
        model=offer
        fields=('product_name','price')

class AddCategoryForm(ModelForm):
    class Meta:
        model=Category
        fields=('title','is_active')    

class AddSubcategoryForm(ModelForm):
    class Meta:
        model=Subcategory
        fields=('category','title')


class offerUpdateForm(ModelForm):
    class Meta:
        model=offer
        fields='__all__'