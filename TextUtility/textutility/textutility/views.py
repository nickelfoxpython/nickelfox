#This python file is created by me Megha

from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    data={
        'name':'Megha',
        'age':23
    }
    return render(request, 'index.html', data)
    # return HttpResponse("Hello Welcome to the welcome page")

def aboutus(request):
    return HttpResponse("I am Megha and I am Python Developer")

def analyze(request):
    djtext=request.POST.get('text','default')
    removepunc=request.POST.get('removepunc','off')
    print(djtext)
    print(removepunc)
    return HttpResponse("remove Punc")

def remove(request):
    return HttpResponse("Remove the punctuation")

def links(request):
    djtext = request.POST.get('text', 'default')
    removepunc = request.POST.get('removepunc', 'off')
    Capitallet=request.POST.get('Capitallet','off')
    removeline=request.POST.get('removeline','off')
    removespace=request.POST.get('removespace','off')
    charactercount=request.POST.get('charactercount','off')
    print(djtext)
    print(removepunc)
    if removepunc=='on':
        punc='''!()=[]{}:;'"\.<>./@#%$^&*_~'''
        analyzed = ""
        for char in djtext:
            if char not in punc:
                analyzed=analyzed+char
        params={
            'purpose':'Removed Punctuation',
            'analyzed_text': analyzed
        }
        # return HttpResponse("remove punctuation")
        return render(request,'analyze.html', params)

    elif removeline=='on':
        analyzed = ""
        for char in djtext:
            if char !='\n':
                analyzed=analyzed+char
        params={
            'purpose':'Remove new line',
            'analyzed_text': analyzed
        }
        # return HttpResponse("remove punctuation")
        return render(request,'analyze.html', params)
    elif removespace=='on':
        analyzed = ""
        for index,char in enumerate(djtext):
            if djtext[index]==" " and djtext[index+1]==" ":
                pass
            else:
                analyzed=analyzed+char
        params={
            'purpose':'Remove extra space from line',
            'analyzed_text': analyzed
        }
        # return HttpResponse("remove punctuation")
        return render(request,'analyze.html', params)
    elif charactercount=='on':
        count=0
        for char in djtext:
            count+=1
        params={
            'purpose':'Remove extra space from line',
            'analyzed_text': count
        }
        # return HttpResponse("remove punctuation")
        return render(request,'analyze.html', params)
    else:
        return HttpResponse("error")
